(function($){

  function getCatalogItems(){
    var request = $.ajax({
        type: 'get',
        url: '/modules/catalog.php',
        data: {'action':'getCatalogItems','limit':$('.js-catalog').data('itemsLimit'),'page':$('.js-catalog').data('itemsPage')},
        dataType: 'html',
        beforeSend: function(xhr) {
          $('body').append('<div class="loading"></div>');
        }
      });
      request.done(function(html) {
        $('.loading').remove();
        var page = $('.js-catalog').data('itemsPage');
        
        $('.js-catalog').append(html);

        $('html, body').animate({
            scrollTop: $("#page-"+page).offset().top
        }, 500);
      });

      request.fail(function(jqXHR, textStatus) {
        console.log('Error. '+textStatus);
      });
    return false;
  }
  getCatalogItems();

  $(document).on('click','.js-pagination button',function(){
    $('.js-pagination').remove();
    $('.js-catalog').data('itemsPage',$('.js-catalog').data('itemsPage')+1);
    getCatalogItems();
  });


})(jQuery);