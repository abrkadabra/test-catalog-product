<div class="container" id="page-<?=$page?>">
	<div class="row">
		<?if($page>1){?>
		<div class="col-md-12 page text-center">
			<div class="alert alert-primary" role="alert">
				
				<span>Старница #<?=$page?></span>
				
			</div>
		</div>
		<?}?>
		<!-- BEGIN PRODUCTS -->

		<?foreach($arPageItems[$page-1] AS $productId=>$product){?>
			<div class="col-md-3">
				<div class="card mb-4 box-shadow text-center">
					<div class="card-body">
						<a href="<?=$product['url']?>">
							<img class="img-thumbnail" src="<?=$product['img']?>" alt="<?=$product['name']?>"/>
						</a>
						<p>
							<a href="<?=$product['url']?>">
								<?=$product['name']?>
							</a>
						</p>
						<h3 class="card-title pricing-card-title">15р. <small class="text-muted">/ литр</small></h3>
						<button type="button" class="btn btn-lg btn-block btn-danger">Купить</button>
					</div>
				</div>
			</div>
			<?}?>
			<!-- END PRODUCTS -->
		</div>
	</div>
	
	<?if((sizeof($arCatalogItems)-$page*$limit)>0){?>

	<div class="container js-pagination">
		<div class="row">
			<div class="col-md-12 text-center">
				<button class="btn btn-lg btn-primary">Загрузить ещё +<?=sizeof($arCatalogItems)-$page*$limit?></button>
			</div>
		</div>
	</div>

	<?}?>


