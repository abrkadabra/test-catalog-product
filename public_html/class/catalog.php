<?php
class Catalog {

	private $url	= false;
	private $data	= array();
	public  $error 	= false;

	function __construct(){

	}
	function getCatalogData($url,$fields){

		$this->url = $url;
		$this->data = $fields;
		return json_decode(self::request_curl(),true); //array($this->data,$url);
	}
	function request_curl($method='post'){
		if($this->url){
	        $ch = curl_init(); 
	        
	        curl_setopt($ch, CURLOPT_URL,$this->url); // set url to post to

	        if($method=='post'){
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
		        curl_setopt($ch, CURLOPT_POST, 1); // set POST method  
		        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->data)); // add POST fields
	        }else{
	        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
	        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
	        }

	        $result = curl_exec($ch); // run the whole process 
	        $err     = curl_errno($ch);
	        $errmsg  = curl_error($ch);
	        $header  = curl_getinfo($ch); 
	        curl_close($ch);
	        if($err){
	        	var_dump($errmsg);
	        }else{
	            return $result;  
	        }  
		}else{
			echo 'error url';
		}
	}	
} 